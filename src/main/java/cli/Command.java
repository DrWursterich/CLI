package cli;

/**
 * A abstract Class to represent the Model of CLI Input Commands
 * such as Flags or Parsers.<br/>
 * @author Mario Schäper
 */
public abstract class Command {
	protected static int defaultValues = 1;
	protected static String defaultValueSeparator = "-";
	protected String name;
	protected int values;
	protected String valueSeparator;
	protected String overview;
	protected String description;

	/**
	 * Determines the Amount of Values expected by the Action of Commands,
	 * that are invoked without a specific Value.
	 * @param defaultValues the Amount of Values expected
	 * @throws IllegalArgumentException if the Amount is invalid
	 */
	public static void setDefaultValues(final int defaultValues)
			throws IllegalArgumentException {
		Command.checkValues(defaultValues);
		Command.defaultValues = defaultValues;
	}

	/**
	 * Returns the Amount of Values expected by the Action of Commands,
	 * that are invoked without a specific Value.
	 * @return the Amount of Values expected
	 */
	public static int getDefaultValues() {
		return Command.defaultValues;
	}

	/**
	 * Determines the valueSeparator, with which new Flags are instanciated.
	 * Can only contain Whitespace, if thats the only Char.
	 * @param defaultValueSeparator the default valueSeparator
	 * @throws IllegalArgumentException if the valueSeparator is invalid
	 */
	public static void setDefaultValueSeparator(
			final String defaultValueSeparator)
			throws IllegalArgumentException {
		Flag.checkValueSeparator(defaultValueSeparator);
		Command.defaultValueSeparator = defaultValueSeparator;
	}

	/**
	 * Returns the valueSeparator, with which new Flags are instanciated.
	 * @return the valueSeparator
	 */
	public static String getDefaultValueSeparator() {
		return Command.defaultValueSeparator;
	}

	/**
	 * Determines the Name/Prefix of this Command.<br/>
	 * For example, a Command with the Name <em>"-c"</em>
	 * and the Values of <em>0</em>
	 * would have to be formated <em>"-c"</em> to be parsed correctly.
	 * @param name the Name/Prefix
	 * @throws IllegalArgumentException if the Name is invalid
	 */
	public void setName(final String name) throws IllegalArgumentException {
		if (name == null || name.contains(" ") || name.equals("")) {
			throw new IllegalArgumentException("Invalid name \"" + name + "\"");
		}
		this.name = name;
	}

	/**
	 * Returns the Name/Prefix of this Command.<br/>
	 * For example, a Command with the Name <em>"-c"</em>
	 * and the Values of <em>0</em>
	 * would have to be formated <em>"-c"</em> to be parsed correctly.
	 * @return the Name/Prefix
	 */
	public String getName() {
		return this.name;
	}

	public void setOverview(final String overview) {
		this.overview = overview;
	}

	public String getOverview() {
		return this.overview;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public String getDescription() {
		return this.description;
	}

	/**
	 * Determines the Amount of Values expected by the Action of this Command.
	 * @param values the Amount of Values expected
	 * @throws IllegalArgumentException if the Amount is invalid
	 */
	public void setValues(final int values) throws IllegalArgumentException {
		Command.checkValues(values);
		this.values = values;
	}

	/**
	 * Returns the Amount of Values expected by the Action of this Command.
	 * @return the Amount of Values expected
	 */
	public int getValues() {
		return this.values;
	}

	/**
	 * Determines the String, that separates Key and Values of this Flag.
	 * Can only contain Whitespace, if thats the only Char.
	 * @param valueSeparator the String, that seperate Key and Values
	 * @throws IllegalArgumentException if the valueSeparator is invalid
	 */
	public void setValueSeparator(final String valueSeparator)
			throws IllegalArgumentException {
		Flag.checkValueSeparator(valueSeparator);
		this.valueSeparator = valueSeparator;
	}

	/**
	 * Returns the String, that separates Key and Values of this Flag.
	 * @return the String, that seperate Key and Values
	 */
	public String getValueSeparator() {
		return this.valueSeparator;
	}

	/**
	 * Returns the Commands Name.
	 * @return the Name
	 */
	@Override
	public String toString() {
		return this.name;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		return this.name.hashCode() + this.values;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(final Object other) {
		if (other == null || !(other instanceof Flag)) {
			return false;
		}
		Command command = (Command)other;
		return command.name == this.name
				&& command.values == this.values;
	}

	protected static void checkValues(final int values)
			throws IllegalArgumentException {
		if (values < 0) {
			throw new IllegalArgumentException(
					"Invalid amount of values \"" + values + "\"");
		}
	}
}
